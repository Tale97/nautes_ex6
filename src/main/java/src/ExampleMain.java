package src;

import src.customer.Customer;
import src.customer.InterviewRecommendedStrategy;

public class ExampleMain {

    public static void main () {
        Customer interviewCustomer = new Customer(false, new InterviewRecommendedStrategy());
        Customer customerRandomSuggestions = new Customer(false, customer -> Product.getRandom());
    }

}
