package src.customer;

import src.Product;

import java.util.Date;

public class Customer {

    private final boolean isProfilingActive;
    private final GetRecommendedStrategy recommendedStrategy;

    public Customer(final boolean isProfilingActive, final GetRecommendedStrategy getRecommendedStrategy){
        this.isProfilingActive = isProfilingActive;
        this.recommendedStrategy = getRecommendedStrategy;
    }

    public boolean isProfilingActive() {
        return isProfilingActive;
    }

    public Product getRecommended() {
        return recommendedStrategy.getRecommended(this);
    }

    public int getPurchasesSince(final Date date) {
        //implementation not necessary for interview purposes
        return 0;
    }

}
