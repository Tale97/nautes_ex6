package src.customer;

import src.Product;

public interface GetRecommendedStrategy {

    Product getRecommended(final Customer customer);

}
