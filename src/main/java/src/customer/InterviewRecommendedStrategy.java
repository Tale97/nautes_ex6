package src.customer;

import src.LikeMyStuff;
import src.Product;
import src.Rel8;
import src.SpecialPromotion;

public class InterviewRecommendedStrategy implements GetRecommendedStrategy{
    @Override
    public Product getRecommended(final Customer customer) {
        String promotionName = SpecialPromotion.getPromotionName();
        if(promotionName != null){
            Product promotionProduct = Product.lookup(promotionName);
            if(promotionProduct != null){
                return promotionProduct;
            }
        }
        return customer.isProfilingActive() ? Rel8.advise(customer): LikeMyStuff.suggest(customer);
    }
}
